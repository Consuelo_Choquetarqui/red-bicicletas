const bicicleta = require('../../models/bicicleta');
var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    res.status(200).json({
       bicicletas: Bicicleta.allBicis 
    });
}

/*
exports.bicicleta_create = function(req, res){
  
  const bici = new Bicicleta();
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lat, req.body.lng];

  Bicicleta.add(bici);

  res.status(200).json({bicicleta: bici});
}
*/

exports.bicicleta_create = function(req, res){
    //console.log(" ENTRA AL CONTROLER UNONO ID:" +  req.body.id); // CCH    
    //console.log(" ENTRA AL CONTROLER UNONO CODE:" + req.body.code); // CCH 
    //console.log(" ENTRA AL CONTROLER UNONO COLOR:" + req.body.color); // CCH 
    //console.log(" ENTRA AL CONTROLER UNONO MODELO:" + req.body.modelo); // CCH  
    //var bici = new Bicicleta( req.body.id, req.body.color, req.body.modelo);   CCH    
    //bici.ubicacion = [req.body.lat, req.body.lng];  CCH
    //--var bici = new Bicicleta(req.code, req.color, req.modelo, req.lat, req.lng)
    // bici.ubicacion = [req.lat, req.lng];   CON ESTO NO FUNCIONA
    //--bici.ubicacion = [req.body.lat, req.body.lng];    

    //var bici = new Bicicleta(_id, req.body.code, req.body.color, req.body.modelo);
    //var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo, req.body.lat, req.body.lng );
   
    const bici = new Bicicleta();
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);
    //console.log(" ENTRA AL CONTROLER DOSDOS ID:" +  bici.id); // CCH    
    //console.log(" ENTRA AL CONTROLER DOSDOS CODE:" + bici.code); // CCH 
    //console.log(" ENTRA AL CONTROLER DOSDOS COLOR:" + bici.color); // CCH 
    //console.log(" ENTRA AL CONTROLER DOSDOS MODELO:" + bici.modelo); // CCH 
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_update= function(req, res) { 
    //console.log('CONTROLLER ' + req.body.id );
    //var bici = Bicicleta.findById(req.body.id);  
    var bici = Bicicleta.findByCode(req.body.id);
    //var bici = Bicicleta.findByCode(10);
    //console.log('RECUPERA ' + bici.code);
    bici.id = req.body.id;
    bici.code = req.body.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
 
    /*console.log('CONTROLLER ID  ' + bici.id);
    console.log('CONTROLLER MOD ' + bici.modelo);
    console.log('CONTROLLER COLOR ' + bici.color);
    console.log('CONTROLLER UBIC ' + bici.ubicacion);
    console.log('CONTROLLER BICI ' + bici );*/
    res.status(202).json({
        bicicleta: Bicicleta.bici //s
    })    
}

exports.bicicleta_delete = function(req, res){       
    //Bicicleta.removeById(req.body.code); CCHH
    Bicicleta.removeByCode(req.body.code);
    res.status(204).send();
} 