const express = require('express');
const router = express.Router();

//const authController = require('../../controllers/api/authControllerAPI');
const authController = require('../../controllers/authControllerAPI');

router.post('/authenticate', authController.authenticate);
router.post('forgetPassword', authController.forgetPassword);

module.exports = router;