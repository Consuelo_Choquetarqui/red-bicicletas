'use strict'

const mongoose = require('mongoose');  // PONER CONST
const Schema = mongoose.Schema;

const bicicletaSchema = new Schema({        
     code: Number,
     color: String,
     modelo: String,
     ubicacion: {
        type: [Number], index: {type: '2dsphere', sparse: true}
    },
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    
    return new this({
         code: code,
         color: color,
         modelo: modelo,
         ubicacion: ubicacion                 
    });
};

bicicletaSchema.methods.toString = function() {
    return 'code: ' + this.code + '| color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb){
       console.log( 'TOTAL MODELO ' + this.find());
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb){
    //console.log('ENTRA AL MODEL BICICLETA ADD : ' + aBici.code);
    //console.log('ENTRA AL MODEL BICICLETA ADD : ' + aBici.color);
    //console.log('ENTRA AL MODEL BICICLETA ADD : ' + aBici.modelo);
    //console.log('ENTRA AL MODEL BICICLETA ADD : ' + aBici.ubicacion);
    return this.create(aBici, cb);

}; 

/*bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, function(err, biciStored){
      if (err) return handleError(err);
    });
  };*/

bicicletaSchema.statics.findByCode = function(aCode, cb){
    //console.log('FINDBYCODE ' + aCode);
    return this.findOne({code: aCode}, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb){
    return this.deleteOne({code: aCode}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*var Bicicleta = function (id, color, modelo, ubicacion){
this.id = id;
this.color = color;
this.modelo = modelo;
this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);    
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
        
}

Bicicleta.removeById = function(aBiciId){
    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
} */

/*var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-34.596932, -58.3808287]);

Bicicleta.add(a);
Bicicleta.add(b);*/

/*module.exports = Bicicleta;*/