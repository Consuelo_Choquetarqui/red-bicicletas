var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');
const { countDocuments } = require('../../models/bicicleta');

var base_url = 'http://localhost:5000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach(function(done) {  
         mongoose.connection.close().then(() => {
           
            originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
                 
            var mongoDB = 'mongodb://localhost/testdb';            
            mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function(){
                //console.log(' are connected to test database');
                //done();
            });
            done();        
         });            
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success) {
            jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;  
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });       
    });

    describe("GET BICICLETAS /", () => {
        it("Status 200", (done) => { 
            originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;                      
     
            base_url =  'http://localhost:3000/api/bicicletas';         
                        
            request.get(base_url, function(error, response, body ) {                
                var result = JSON.parse(body); 
                expect(response.statusCode).toBe(200);  
                expect(Object.keys(result).length).toBe(0)
                //expect(result.bicicletas.length).toBe(0);                    
                done();                
            }); 
        });       
    }); 

    describe("POST BICILETAS / create", () => {
        it("Status 200", (done) => {            
            var headers = {'content-type' : 'application/json'};                
            var aBici = '{"code": 10, "color": "magenta", "modelo": "urbano", "lat": -34, "lng": -54}';
            
            base_url =  'http://localhost:3000/api/bicicletas/create'; 
              
            request.post({
                headers: headers,
                url:     base_url,                 
                body:    aBici
               }, function(error, response, body) {                                         
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body);
                    expect(bici.bicicleta.modelo).toBe("urbano");
                    expect(bici.bicicleta.ubicacion[0]).toBe(-34);
                    expect(bici.bicicleta.ubicacion[1]).toBe(-54);                    
                    //expect(bici.lat).toBe(-34);
                    //expect(bici.lng).toBe(-54);  
                    done();                  
            });  
        });        
    });

    /*describe("DELETE BICICLETAS /delete", () => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000; 
        it("Status 204", (done) => {
            var a = Bicicleta.createInstance(1, "negro", "urbana", [-34.60122424, -58.3861]);
            Bicicleta.add(a, function(err, newBici){
                var headers = {'content-type' : 'application/json'};
                done();
            });
        });
    }); */


 describe(' BICICLETAS /delete', () => {
    it('Status 200', (done)=>{
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;                      

        //const a = new Bicicleta(1, 'azul', 'sport', [-38.01986035440169, -57.54397278410175]);
        //var aBici = '{"code": 10, "color": "rosado", "modelo": "urbano", "lat": -34, "lng": -54}';
        
        //var b = new Bicicleta(2, 'rojo', 'urbano', [-38.01986035440169, -57.54397278410175]);
        //var c = new Bicicleta(3, 'verde', 'mountain', [-38.01986035440169, -57.54397278410175]);
        const a = new Bicicleta();
        a.code = 10;
        a.color = "rosado";
        a.modelo = "femenino";
        a.ubicacion = [-38.01986035440169, -57.54397278410175];
        Bicicleta.add(a);
              
        const b = new Bicicleta();
        b.code = 20;
        b.color = "turqueza";
        b.modelo = "femenino";
        b.ubicacion = [-38.01986035440169, -57.54397278410175];
        Bicicleta.add(b);
        
        var headers = {'content-type' : 'application/json'};
        var abici = '{"code": 10}';
        request.delete({
            headers: headers,
            url: 'http://localhost:3000/api/bicicletas/delete',
            body: abici
        }, function(error, response, body){
            expect(response.statusCode).toBe(204);
            expect(Bicicleta.allBicis.length).toBe(1);
            done();
        });
    });
 });



  /* describe(' BICICLETAS /update', () => {
    it('Status 200', (done)=>{               
       
        const a = new Bicicleta();
        a.code = 10;
        a.color = "amarillo";
        a.modelo = "mudanza";
        a.ubicacion = [-38.01986035440169, -57.54397278410175];
        Bicicleta.add(a);

              
        var biciUpdate = '{"code": 20, "color": "azul", "modelo": "juvenil", "lat": -34, "lng": -54}';
        var headers = {'content-type' : 'application/json'};
        
        request.put({
            headers: headers,
            url: 'http://localhost:3000/api/bicicletas/update',
            body: biciUpdate,
        }, function(error, response, body){
            console.log('ERROR ' + error);
            console.log('UPDAT ' + response.statusCode);
            console.log('BODY color ' + body.color);
            expect(response.statusCode).toBe(202);
            //expect(Bicicleta.findById(10).color).toBe("azul");
            expect(Bicicleta.findByCode(10).color).toBe("azul");
            //expect(Bicicleta.findByCode(10).modelo).toBe("mudanza");
            done();
        });
    });
 }); */

     describe('PUT bicicletas/update', () => {

        it('Deberia actualizar una bicicleta', (done) => {
            const headers = { 'content-type': 'application/json'};
            const bici = `{"id":1, "color":"verde", "modelo":"nueva", "lat":6.230833, "lng":75.590553}`;
            const biciUpdated =  `{"id":1, "color":"rojo", "modelo":"especial", "lat":6.230833, "lng":75.590553}`;

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: bici
                }, function(err, res, body ) {
                    expect(res.statusCode).toBe(200);
                    const bici = JSON.parse(body).bicicleta;
                    expect(bici.color).toBe('verde');
                    expect(bici.ubicacion[0]).toBe(6.230833);
                    expect(bici.ubicacion[1]).toBe(75.590553);
                   
                    request.put({
                        headers: headers,
                        url: 'http://localhost:3000/api/bicicletas/update',
                        body: biciUpdated
                        }, function(err, res){
                            
                            expect(res.statusCode).toBe(202); 
                            request.get({    
                                url: 'http://localhost:3000/api/bicicletas',
                                }, function(err, res, body ){
                                const result = JSON.parse(body).bicicleta;
                                //expect(res.statusCode).toBe(200);  
                                done();
                             });
                         });
                 });
         });
     }); 
 }); 


/*describe('Biciclea API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas',
                  function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
             var headers = {'content-type' : 'application/json'};
             var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
             request.post({
                headers: headers,
                url:     'http://localhost:3000/api/bicicletas/create',
                body: aBici 
              }, function(error, response, body) {
                 expect(response.statusCode).toBe(200);
                 expect(Bicicleta.findById(10).color).toBe("rojo");
                 done();
            });
        });
    });
}); */